package me.ndjoe.plantjournal2;

import android.app.Application;
import me.ndjoe.plantjournal2.dagger.component.DaggerServicesComponent;
import me.ndjoe.plantjournal2.dagger.component.ServicesComponent;
import me.ndjoe.plantjournal2.dagger.module.HandlersModule;
import me.ndjoe.plantjournal2.dagger.module.RealmModule;

public class PlantJournal extends Application {
  private static ServicesComponent mServicesComponent;

  public static ServicesComponent getServicesComponent() {
    return mServicesComponent;
  }

  @Override public void onCreate() {
    super.onCreate();

    mServicesComponent = DaggerServicesComponent.builder()
        .handlersModule(new HandlersModule())
        .realmModule(new RealmModule(this))
        .build();
  }
}
