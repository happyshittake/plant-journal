package me.ndjoe.plantjournal2.ui.base;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;
import me.ndjoe.plantjournal2.R;

public abstract class BaseActivity extends RxAppCompatActivity {

  protected void insertFragment(BaseFragment fragment) {
    FragmentManager fragmentManager = getSupportFragmentManager();
    Fragment fragmentContainer = fragmentManager.findFragmentById(R.id.container);

    if (fragmentContainer == null) {
      fragmentManager.beginTransaction().add(R.id.container, fragment).commit();
    }
  }
}
