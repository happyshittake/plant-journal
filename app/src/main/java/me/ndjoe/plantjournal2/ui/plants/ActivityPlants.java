package me.ndjoe.plantjournal2.ui.plants;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import au.com.bytecode.opencsv.CSVReader;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.afollestad.materialdialogs.MaterialDialog;
import io.realm.Realm;
import io.realm.RealmList;
import ir.sohreco.androidfilechooser.ExternalStorageNotAvailableException;
import ir.sohreco.androidfilechooser.FileChooserDialog;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.UUID;
import javax.inject.Inject;
import me.ndjoe.plantjournal2.R;
import me.ndjoe.plantjournal2.dagger.component.DaggerServicesComponent;
import me.ndjoe.plantjournal2.dagger.module.HandlersModule;
import me.ndjoe.plantjournal2.dagger.module.RealmModule;
import me.ndjoe.plantjournal2.data.model.Journal;
import me.ndjoe.plantjournal2.data.model.Plant;
import me.ndjoe.plantjournal2.ui.base.BaseActivity;

public class ActivityPlants extends BaseActivity implements FileChooserDialog.ChooserListener {
  PlantsFragment mFragment;
  @Inject Realm mRealm;
  @BindView(R.id.toolbar) Toolbar mToolbar;

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    DaggerServicesComponent.builder()
        .handlersModule(new HandlersModule())
        .realmModule(new RealmModule(getApplication()))
        .build()
        .inject(this);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);
    setSupportActionBar(mToolbar);
  }

  @Override public void onContentChanged() {
    super.onContentChanged();
    mFragment = PlantsFragment.newInstance();
    insertFragment(PlantsFragment.newInstance());
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.plant_menu, menu);
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();

    switch (id) {
      case R.id.action_seed_data:
        importCSV();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    if (requestCode == 200) {
      if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        importCSV();
      }
    }
  }

  private void importCSV() {
    int permissionCheck =
        ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
    if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
      ActivityCompat.requestPermissions(this,
          new String[] { Manifest.permission.READ_EXTERNAL_STORAGE }, 200);
    } else {
      FileChooserDialog.Builder builder =
          new FileChooserDialog.Builder(FileChooserDialog.ChooserType.FILE_CHOOSER, this).setTitle(
              "Select Csv File:").setFileFormats(new String[] { ".csv" });
      new MaterialDialog.Builder(this).positiveText("From default data")
          .negativeText("From storage")
          .neutralText("Cance")
          .onPositive((dialog, which) -> {
            onSelect(getResources().openRawResource(R.raw.tanaman));
            dialog.dismiss();
          })
          .onNeutral((dialog, which) -> {
            dialog.dismiss();
          })
          .onNegative((dialog, which) -> {
            try {
              builder.build().show(getSupportFragmentManager(), null);
              dialog.dismiss();
            } catch (ExternalStorageNotAvailableException e) {
              e.printStackTrace();
            }
          })
          .build()
          .show();
    }
  }

  @Override public void onSelect(String path) {
    MaterialDialog dialog = new MaterialDialog.Builder(this).progress(true, 30).build();
    dialog.show();
    try {
      InputStream stream = openFileInput(path);
      InputStreamReader csvStreamReader = new InputStreamReader(stream);
      CSVReader csvReader = new CSVReader(csvStreamReader);
      csvReader.readNext();
      String[] line2 = csvReader.readNext();
      String[] line3 = csvReader.readNext();
      RealmList<Plant> plants = new RealmList<>();
      for (int i = 2; i < line2.length; i++) {
        if (line2[i].length() > 0) {
          Plant plant = new Plant();
          plant.setName("Tanaman " + line2[i] + " - a");
          plant.setId(UUID.randomUUID().toString());
          plants.add(plant);
          Plant plant2 = new Plant();
          plant2.setName("Tanaman " + line2[i] + " - b");
          plant2.setId(UUID.randomUUID().toString());
          plants.add(plant2);
        }
      }

      mFragment.receiveNewPlants(plants);

      csvReader.readNext();
      String[] line;
      SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy");
      RealmList<Journal> journals = new RealmList<>();
      int lineNum = 0;
      while ((line = csvReader.readNext()) != null) {
        int idx = 0;
        for (int i = 2; i < line.length; i++) {
          Plant plant = plants.get(idx);
          Journal journal = new Journal();
          journal.setId(UUID.randomUUID().toString());
          journal.setDate(format.parse(line[1]));
          journal.setHeight(Float.valueOf(line[i]));
          journal.setPlant(plant);
          journals.add(journal);
          idx = idx + 1;
        }
        lineNum = lineNum + 1;
      }
      mFragment.receiveNewJournals(journals);
    } catch (IOException | ParseException e) {
      e.printStackTrace();
    }
  }

  public void onSelect(InputStream inputStream) {
    MaterialDialog dialog = new MaterialDialog.Builder(this).progress(true, 30).build();
    dialog.show();
    try {
      InputStreamReader csvStreamReader = new InputStreamReader(inputStream);
      CSVReader csvReader = new CSVReader(csvStreamReader);
      csvReader.readNext();
      String[] line2 = csvReader.readNext();
      String[] line3 = csvReader.readNext();
      RealmList<Plant> plants = new RealmList<>();
      for (int i = 2; i < line2.length; i++) {
        if (line2[i].length() > 0) {
          Plant plant = new Plant();
          plant.setName("Tanaman " + line2[i] + " - a");
          plant.setId(UUID.randomUUID().toString());
          plants.add(plant);
          Log.i("Plant Activity", plant.toString());
          Plant plant2 = new Plant();
          plant2.setName("Tanaman " + line2[i] + " - b");
          plant2.setId(UUID.randomUUID().toString());
          plants.add(plant2);
          Log.i("Plant Activity", plant2.toString());
        }
      }
      Log.i("Plant Activity", plants.toString());
      mRealm.executeTransaction(realm -> realm.copyToRealmOrUpdate(plants));

      csvReader.readNext();
      String[] line;
      SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy");
      HashMap<Integer, RealmList<Journal>> map = new HashMap<>();
      int lineNum;
      while ((line = csvReader.readNext()) != null) {
        lineNum = 0;
        for (int i = 2; i < line.length; i++) {
          Journal journal = new Journal();
          journal.setId(UUID.randomUUID().toString());
          journal.setDate(format.parse(line[1]));
          journal.setHeight(Float.valueOf(line[i]));
          Log.i("Plant activity", journal.toString());
          if (!map.containsKey(lineNum)) {
            RealmList<Journal> list = new RealmList<>();
            list.add(journal);
            map.put(lineNum, list);
          } else {
            map.get(lineNum).add(journal);
          }
          lineNum = lineNum + 1;
        }
      }
      mRealm.executeTransactionAsync(realm -> {
        for (int i = 0; i < plants.size(); i++) {
          RealmList<Journal> js = map.get(i);
          Plant plant = plants.get(i);
          realm.copyToRealmOrUpdate(js);
          Plant realmPlant = realm.where(Plant.class).equalTo("id", plant.getId()).findFirst();
          for (Journal journal : js) {
            Journal realmJournal =
                realm.where(Journal.class).equalTo("id", journal.getId()).findFirst();
            realmPlant.getJournals().add(realmJournal);
            realmJournal.setPlant(realmPlant);
          }
        }
      }, dialog::dismiss);
    } catch (IOException | ParseException e) {
      e.printStackTrace();
      dialog.dismiss();
    }
  }

  @Override protected void onStop() {
    super.onStop();
  }
}
