package me.ndjoe.plantjournal2.ui.plants;

import io.realm.RealmList;
import java.util.List;
import me.ndjoe.plantjournal2.data.model.Journal;
import me.ndjoe.plantjournal2.data.model.Plant;
import me.ndjoe.plantjournal2.ui.base.BasePresenter;
import me.ndjoe.plantjournal2.ui.base.BaseView;

public interface PlantsContract {
  interface Presenter extends BasePresenter {
    void loadPlants();

    void addNewPlant(String name);

    Journal getLatestJournal(Plant plant);

    void receiveNewPlants(RealmList<Plant> plants);

    void receiveNewJournals(RealmList<Journal> journals);
  }

  interface View extends BaseView<Presenter> {
    void showPlants(List<Plant> plants);

    void receiveNewPlants(RealmList<Plant> plants);

    void receiveNewJournals(RealmList<Journal> journals);
  }
}
