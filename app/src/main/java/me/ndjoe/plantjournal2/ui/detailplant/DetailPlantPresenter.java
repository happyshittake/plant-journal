package me.ndjoe.plantjournal2.ui.detailplant;

import io.realm.Sort;
import me.ndjoe.plantjournal2.data.model.Journal;
import me.ndjoe.plantjournal2.data.model.Plant;
import me.ndjoe.plantjournal2.data.service.JournalService;
import me.ndjoe.plantjournal2.data.service.PlantService;
import rx.Subscription;
import rx.subjects.PublishSubject;

public class DetailPlantPresenter implements DetailPlantContract.Presenter {
  private DetailPlantContract.View mView;
  private PlantService mPlantService;
  private JournalService mJournalService;
  private Plant mPlant;

  private Subscription mListJournalSubscription;
  private Subscription mHighestJournalSubscription;
  private Subscription mPlantSubscription;
  private Subscription mAddJournalSubscription;
  private PublishSubject<Plant> mPlantBehaviorSubject = PublishSubject.create();

  public DetailPlantPresenter(DetailPlantContract.View view, PlantService plantService,
      JournalService journalService) {
    mView = view;
    mPlantService = plantService;
    mJournalService = journalService;
  }

  @Override public void loadJournals(String plantId) {
    unsubscribe(mPlantSubscription);

    mPlantSubscription = mPlantService.getPlant(plantId).flatMap(plant -> {
      mPlant = plant;
      return mJournalService.getJournalSortedDateByPlant(plant, Sort.ASCENDING);
    }).subscribe(journals -> {
      mView.showChart(journals);
    });
  }

  @Override public void addJournal(Journal journal) {
    unsubscribe(mAddJournalSubscription);
    mAddJournalSubscription = mPlantService.addJournal(mPlant, journal).subscribe(plant -> {

    });
  }

  @Override public void getHeighestJournal(String plantId) {
    unsubscribe(mHighestJournalSubscription);
    mHighestJournalSubscription = mPlantService.getPlant(plantId).flatMap(plant -> {
      mPlant = plant;
      return mJournalService.getJournalsSortedHeightByPlant(plant, Sort.DESCENDING);
    }).subscribe(journals -> {
      mView.showHeighestJournal(journals.get(0));
    });
  }

  private void unsubscribe(Subscription s) {
    if (s != null) {
      s.unsubscribe();
    }
  }
}
