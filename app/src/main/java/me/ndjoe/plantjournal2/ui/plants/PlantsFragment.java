package me.ndjoe.plantjournal2.ui.plants;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.afollestad.materialdialogs.MaterialDialog;
import io.realm.RealmList;
import java.util.List;
import javax.inject.Inject;
import me.ndjoe.plantjournal2.PlantJournal;
import me.ndjoe.plantjournal2.R;
import me.ndjoe.plantjournal2.dagger.component.DaggerPlantComponent;
import me.ndjoe.plantjournal2.dagger.module.PlantsPresenterModule;
import me.ndjoe.plantjournal2.data.model.Journal;
import me.ndjoe.plantjournal2.data.model.Plant;
import me.ndjoe.plantjournal2.ui.base.BaseFragment;
import me.ndjoe.plantjournal2.ui.detailplant.DetailPlantActivity;
import rx.Subscription;

public class PlantsFragment extends BaseFragment implements PlantsContract.View {
  @Inject PlantsContract.Presenter mPresenter;
  @BindView(R.id.fragment_plants_recycler) RecyclerView mRecyclerView;
  LinearLayoutManager mLinearLayoutManager;
  PlantsAdapter mPlantsAdapter;
  @BindView(R.id.fragment_plants_fab) FloatingActionButton mFab;
  private Subscription recyclerAdapterClikSubscriber;

  public static PlantsFragment newInstance() {
    return new PlantsFragment();
  }

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    DaggerPlantComponent.builder()
        .servicesComponent(PlantJournal.getServicesComponent())
        .plantsPresenterModule(new PlantsPresenterModule(this))
        .build()
        .inject(this);
  }

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_plants, container, false);
    ButterKnife.bind(this, v);

    mLinearLayoutManager = new LinearLayoutManager(getActivity());
    mPlantsAdapter = new PlantsAdapter(getActivity(), mPresenter);
    mRecyclerView.setLayoutManager(mLinearLayoutManager);
    mRecyclerView.setAdapter(mPlantsAdapter);
    recyclerAdapterClikSubscriber = mPlantsAdapter.getPositionClick().subscribe(plant -> {
      Intent intent = new Intent(getActivity(), DetailPlantActivity.class);
      intent.putExtra("PLANT_ID", plant.getId());
      startActivity(intent);
    });

    mFab.setOnClickListener(
        view -> new MaterialDialog.Builder(getContext()).title("Buat tanaman baru")
            .input("Nama tanaman", null, false, (dialog, input) -> {

            })
            .positiveText("Save")
            .negativeText("Cancel")
            .onPositive((dialog, which) -> {
              String name = dialog.getInputEditText().getText().toString();
              mPresenter.addNewPlant(name);
              dialog.dismiss();
            })
            .build()
            .show());

    return v;
  }

  @Override public void onDestroyView() {
    if (recyclerAdapterClikSubscriber != null) {
      recyclerAdapterClikSubscriber.unsubscribe();
    }
    super.onDestroyView();
  }

  @Override public void onResume() {
    super.onResume();
    mPresenter.loadPlants();
  }

  @Override public void showPlants(List<Plant> plants) {
    mPlantsAdapter.swapObjects(plants);
  }

  public void receiveNewPlants(RealmList<Plant> plants) {
    mPresenter.receiveNewPlants(plants);
  }

  public void receiveNewJournals(RealmList<Journal> journals) {
    mPresenter.receiveNewJournals(journals);
  }
}
