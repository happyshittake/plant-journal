package me.ndjoe.plantjournal2.ui.plants;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import me.ndjoe.plantjournal2.R;
import me.ndjoe.plantjournal2.data.model.Journal;
import me.ndjoe.plantjournal2.data.model.Plant;
import me.ndjoe.plantjournal2.ui.base.BaseAdapter;
import rx.Observable;
import rx.subjects.PublishSubject;

public class PlantsAdapter extends BaseAdapter<Plant, PlantsAdapter.ViewHolder> {
  private final PublishSubject<Plant> onCLickSubject = PublishSubject.create();
  private Context mContext;
  private PlantsContract.Presenter mPresenter;

  public PlantsAdapter(Context context, PlantsContract.Presenter presenter) {
    super(new ArrayList<>());
    mContext = context;
    mPresenter = presenter;
  }

  @Override public void onBindViewHolder(ViewHolder holder, Plant item) {
    holder.mName.setText(item.getName());
    holder.mLastActivity.setText("");
    holder.mCardView.setOnClickListener(view -> onCLickSubject.onNext(item));
    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy");
    Journal journal = mPresenter.getLatestJournal(item);
    if (journal != null) {
      holder.mLastActivity.setText(
          String.format(Locale.getDefault(), "tgl: %s - %f cm", format.format(journal.getDate()),
              journal.getHeight()));
    }
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(mContext).inflate(R.layout.plant_adapter, parent, false);
    return new ViewHolder(v);
  }

  public Observable<Plant> getPositionClick() {
    return onCLickSubject.asObservable();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.adapter_plant_card_view) CardView mCardView;
    @BindView(R.id.adapter_plant_last_activity) TextView mLastActivity;
    @BindView(R.id.adapter_plant_name) TextView mName;

    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
