package me.ndjoe.plantjournal2.ui.detailplant;

import java.util.List;
import me.ndjoe.plantjournal2.data.model.Journal;
import me.ndjoe.plantjournal2.data.model.Plant;
import me.ndjoe.plantjournal2.ui.base.BasePresenter;
import me.ndjoe.plantjournal2.ui.base.BaseView;

public interface DetailPlantContract {
  interface Presenter extends BasePresenter {
    void loadJournals(String plantId);

    void addJournal(Journal journal);

    void getHeighestJournal(String plantId);
  }

  interface View extends BaseView<Presenter> {
    void showChart(List<Journal> journals);

    void showPlantName(Plant plant);

    void showHeighestJournal(Journal journal);
  }
}
