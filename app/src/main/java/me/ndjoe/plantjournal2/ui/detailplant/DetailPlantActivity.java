package me.ndjoe.plantjournal2.ui.detailplant;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.realm.implementation.RealmLineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import javax.inject.Inject;
import me.ndjoe.plantjournal2.PlantJournal;
import me.ndjoe.plantjournal2.R;
import me.ndjoe.plantjournal2.dagger.component.DaggerDetailPlantComponent;
import me.ndjoe.plantjournal2.dagger.module.DetailPlantPresenterModule;
import me.ndjoe.plantjournal2.data.model.Journal;
import me.ndjoe.plantjournal2.data.model.Plant;
import me.ndjoe.plantjournal2.ui.base.BaseActivity;

public class DetailPlantActivity extends BaseActivity implements DetailPlantContract.View {
  @Inject DetailPlantContract.Presenter mPresenter;
  @Inject Realm mRealm;
  @BindView(R.id.toolbar) Toolbar mToolbar;
  @BindView(R.id.barchart) LineChart mLineChart;
  @BindView(R.id.plant_name) TextView mPlantName;
  @BindView(R.id.plant_max_height) TextView mPlantHeight;
  @BindView(R.id.detail_plant_fab) FloatingActionButton mFloatingActionButton;
  private Plant mPlant;

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    DaggerDetailPlantComponent.builder()
        .servicesComponent(PlantJournal.getServicesComponent())
        .detailPlantPresenterModule(new DetailPlantPresenterModule(this))
        .build()
        .inject(this);
    setContentView(R.layout.activity_detail_plant);
    String plantId = getIntent().getStringExtra("PLANT_ID");
    mPlant = mRealm.where(Plant.class).equalTo("id", plantId).findFirst();
    showRealmChart();
    showPlantName(mPlant);
    showHeighestJournal(mPlant.getJournals().sort("height", Sort.DESCENDING).get(0));
  }

  @Override public void onContentChanged() {
    super.onContentChanged();
    ButterKnife.bind(this);
    mFloatingActionButton.setOnClickListener(
        view -> new MaterialDialog.Builder(this).customView(R.layout.dialog_add_journal, false)
            .positiveText("Save")
            .negativeText("Cancel")
            .onPositive((dialog, which) -> {
              View customView = dialog.getCustomView();
              DatePicker picker =
                  (DatePicker) customView.findViewById(R.id.dialog_add_journal_datepicker);
              EditText heightField =
                  (EditText) customView.findViewById(R.id.dialog_add_journal_height_field);
              SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");

              try {
                Date date = formatter.parse(
                    String.format(Locale.getDefault(), "%d-%d-%d", picker.getMonth() + 1,
                        picker.getDayOfMonth(), picker.getYear()));
                Journal journal = new Journal();
                journal.setId(UUID.randomUUID().toString());
                journal.setHeight(Float.valueOf(heightField.getText().toString()));
                journal.setDate(date);
                mRealm.executeTransactionAsync(realm -> {
                  mPlant.getJournals().add(journal);
                });
              } catch (ParseException e) {
                e.printStackTrace();
              }
              dialog.dismiss();
            })
            .onNegative((dialog, which) -> dialog.dismiss())
            .show());
  }

  public void showRealmChart() {
    RealmResults<Journal> journals = mRealm.where(Journal.class)
        .equalTo("plant.id", mPlant.getId())
        .findAllSorted("date", Sort.ASCENDING);
    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    IAxisValueFormatter formatter =
        (value, axis) -> format.format(journals.get((int) value).getDate());

    RealmLineDataSet<Journal> realmLineDataSet = new RealmLineDataSet<>(journals, "height");
    realmLineDataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
    realmLineDataSet.setLabel("Tinggi Tanaman");
    realmLineDataSet.setLineWidth(1.8f);
    realmLineDataSet.setCircleRadius(3.6f);
    ArrayList<ILineDataSet> dataSets = new ArrayList<>();
    dataSets.add(realmLineDataSet);
    LineData lineData = new LineData(dataSets);

    mLineChart.setData(lineData);
    mLineChart.getXAxis().setValueFormatter(formatter);
    mLineChart.animateY(1400, Easing.EasingOption.EaseInOutQuart);
  }

  @Override public void showChart(List<Journal> journals) {

  }

  @Override public void showPlantName(Plant plant) {
    mPlantName.setText(String.format(Locale.ENGLISH, "Nama: %s", plant.getName()));
  }

  @Override public void showHeighestJournal(Journal journal) {
    mPlantHeight.setText(String.format(Locale.ENGLISH, "Tinggi: %f", journal.getHeight()));
  }
}
