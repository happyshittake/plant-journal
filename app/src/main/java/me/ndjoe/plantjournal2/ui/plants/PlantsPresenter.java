package me.ndjoe.plantjournal2.ui.plants;

import io.realm.RealmList;
import io.realm.Sort;
import java.util.UUID;
import me.ndjoe.plantjournal2.data.model.Journal;
import me.ndjoe.plantjournal2.data.model.Plant;
import me.ndjoe.plantjournal2.data.service.JournalService;
import me.ndjoe.plantjournal2.data.service.PlantService;
import rx.Subscription;

public class PlantsPresenter implements PlantsContract.Presenter {
  private PlantsContract.View mView;
  private PlantService mPlantService;
  private JournalService mJournalService;

  private Subscription mListPlantSubscription;
  private Subscription mAddPlantSubscription;
  private Subscription mAddJournalSubscription;

  public PlantsPresenter(PlantsContract.View view, PlantService plantService,
      JournalService journalService) {
    mView = view;
    mPlantService = plantService;
    mJournalService = journalService;
  }

  @Override public void loadPlants() {
    unsubscribe(mListPlantSubscription);

    mListPlantSubscription =
        mPlantService.listPlants().subscribe(mView::showPlants, Throwable::printStackTrace);
  }

  private void unsubscribe(Subscription s) {
    if (s != null) {
      s.unsubscribe();
    }
  }

  @Override public void addNewPlant(String name) {
    unsubscribe(mAddPlantSubscription);
    Plant plant = new Plant();
    plant.setName(name);
    plant.setId(UUID.randomUUID().toString());
    mAddPlantSubscription = mPlantService.addPlant(plant).subscribe(s -> {

    }, Throwable::printStackTrace);
  }

  @Override public Journal getLatestJournal(Plant plant) {
    final Journal[] journal = { null };
    mPlantService.getPlant(plant.getId())
        .filter(plant1 -> plant1.getJournals().size() > 0)
        .subscribe(plant1 -> {
          journal[0] = plant1.getJournals().sort("date", Sort.DESCENDING).first();
        }, Throwable::printStackTrace);
    return journal[0];
  }

  @Override public void receiveNewPlants(RealmList<Plant> plants) {
    unsubscribe(mAddPlantSubscription);

    mAddPlantSubscription = mPlantService.addMultiplePlants(plants).subscribe(s -> {

    }, Throwable::printStackTrace);
  }

  @Override public void receiveNewJournals(RealmList<Journal> journals) {
    unsubscribe(mAddJournalSubscription);

    mAddJournalSubscription = mJournalService.addMultipleJournal(journals).subscribe(s -> {

    }, Throwable::printStackTrace);
  }
}
