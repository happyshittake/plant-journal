package me.ndjoe.plantjournal2.data.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Plant extends RealmObject implements BaseModel {
  @PrimaryKey private String id;
  private String name;
  private RealmList<Journal> journals;

  public RealmList<Journal> getJournals() {
    return journals;
  }

  public void setJournals(RealmList<Journal> journals) {
    this.journals = journals;
  }

  @Override public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override public String toString() {
    return "Plant{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", journals=" + journals +
        '}';
  }
}
