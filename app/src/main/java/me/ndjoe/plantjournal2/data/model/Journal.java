package me.ndjoe.plantjournal2.data.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import java.util.Date;

public class Journal extends RealmObject implements BaseModel {
  @PrimaryKey private String id;
  private float height;
  private Date date;
  private Plant plant;

  public Plant getPlant() {
    return plant;
  }

  public void setPlant(Plant plant) {
    this.plant = plant;
  }

  public float getHeight() {
    return height;
  }

  public void setHeight(float height) {
    this.height = height;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  @Override public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  @Override public String toString() {
    return "Journal{" +
        "id=" + id +
        ", height=" + height +
        ", date=" + date +
        ", plant=" + plant +
        '}';
  }
}
