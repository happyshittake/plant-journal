package me.ndjoe.plantjournal2.data.model;

import io.realm.RealmModel;

public interface BaseModel extends RealmModel {
  String getId();
}
