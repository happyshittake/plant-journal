package me.ndjoe.plantjournal2.data.service;

import io.realm.RealmList;
import java.util.List;
import me.ndjoe.plantjournal2.data.model.Journal;
import me.ndjoe.plantjournal2.data.model.Plant;
import rx.Observable;

public interface PlantService {
  Observable<Plant> getPlant(String id);

  Observable<Plant> addPlant(Plant plant);

  Observable<Plant> updatePlant(Plant plant);

  Observable<Void> deletePlant(Plant plant);

  Observable<List<Plant>> listPlants();

  Observable<RealmList<Plant>> addMultiplePlants(RealmList<Plant> plants);

  Observable<Plant> addJournal(Plant plant, Journal journal);
}
