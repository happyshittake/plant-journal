package me.ndjoe.plantjournal2.data.handler;

import io.realm.Realm;
import io.realm.RealmList;
import java.util.List;
import me.ndjoe.plantjournal2.data.model.Journal;
import me.ndjoe.plantjournal2.data.model.Plant;
import me.ndjoe.plantjournal2.data.service.PlantService;
import rx.Observable;

public class PlantHandler extends BaseRealmHandler<Plant> implements PlantService {
  public PlantHandler(Realm realm) {
    super(realm);
  }

  @Override public Class<Plant> getModelClass() {
    return Plant.class;
  }

  @Override public Observable<Plant> getPlant(String id) {
    return getObject(id);
  }

  @Override public Observable<Plant> addPlant(Plant plant) {
    return createObject(plant);
  }

  @Override public Observable<Plant> updatePlant(Plant plant) {
    return updateObject(plant);
  }

  @Override public Observable<Void> deletePlant(Plant plant) {
    return deleteObject(plant);
  }

  @Override public Observable<List<Plant>> listPlants() {
    return listObjects();
  }

  @Override public Observable<RealmList<Plant>> addMultiplePlants(RealmList<Plant> plants) {
    return doAsync(plants, mRealm::copyToRealmOrUpdate);
  }

  @Override public Observable<Plant> addJournal(Plant plant, Journal journal) {
    return doAsync(plant, p -> {
      p.getJournals().add(journal);
    });
  }
}
