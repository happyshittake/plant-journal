package me.ndjoe.plantjournal2.data.service;

import io.realm.RealmList;
import io.realm.Sort;
import java.util.List;
import me.ndjoe.plantjournal2.data.model.Journal;
import me.ndjoe.plantjournal2.data.model.Plant;
import rx.Observable;

public interface JournalService {
  Observable<List<Journal>> getJournalsSortedHeightByPlant(Plant plant, Sort sort);

  Observable<List<Journal>> getJournalSortedDateByPlant(Plant plant, Sort sort);

  Observable<Journal> addJournal(Journal journal);

  Observable<Journal> updateJournal(Journal journal);

  Observable<Void> deleteJournal(Journal journal);

  Observable<RealmList<Journal>> addMultipleJournal(RealmList<Journal> journals);
}
