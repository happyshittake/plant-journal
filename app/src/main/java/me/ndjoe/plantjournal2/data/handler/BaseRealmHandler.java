package me.ndjoe.plantjournal2.data.handler;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.exceptions.RealmException;
import java.util.List;
import me.ndjoe.plantjournal2.data.model.BaseModel;
import rx.Emitter;
import rx.Observable;
import rx.functions.Action1;

public abstract class BaseRealmHandler<T extends RealmObject & BaseModel> extends BaseHandler<T> {
  protected final Realm mRealm;

  public BaseRealmHandler(Realm realm) {
    mRealm = realm;
  }

  public abstract Class<T> getModelClass();

  Observable<T> doAsync(T object, Action1<T> action1) {
    return Observable.fromEmitter(e -> {
      mRealm.beginTransaction();
      try {
        action1.call(object);
        mRealm.commitTransaction();
      } catch (RuntimeException exception) {
        mRealm.cancelTransaction();
        e.onError(new RealmException("Error during transaction.", exception));
        return;
      } catch (Error error) {
        mRealm.cancelTransaction();
        e.onError(error);
        return;
      }
      e.onNext(object);
      e.onCompleted();
    }, Emitter.BackpressureMode.BUFFER);
  }

  Observable<RealmList<T>> doAsync(RealmList<T> object, Action1<RealmList<T>> action1) {
    return Observable.fromEmitter(e -> {
      mRealm.beginTransaction();
      try {
        action1.call(object);
        mRealm.commitTransaction();
      } catch (RuntimeException exception) {
        mRealm.cancelTransaction();
        e.onError(new RealmException("Error during transaction.", exception));
        return;
      } catch (Error error) {
        mRealm.cancelTransaction();
        e.onError(error);
        return;
      }
      e.onNext(object);
      e.onCompleted();
    }, Emitter.BackpressureMode.BUFFER);
  }

  @Override Observable<T> getObject(String id) {
    return mRealm.where(getModelClass()).equalTo("id", id).findAll().first().asObservable();
  }

  @Override Observable<List<T>> listObjects() {
    return mRealm.where(getModelClass()).findAll().asObservable().map(mRealm::copyFromRealm);
  }

  @Override Observable<T> createObject(T object) {
    return doAsync(object, mRealm::copyToRealmOrUpdate);
  }

  @Override Observable<T> updateObject(T object) {
    return doAsync(object, mRealm::copyToRealmOrUpdate);
  }

  @Override Observable<Void> deleteObject(T object) {
    return doAsync(object, o -> mRealm.where(getModelClass())
        .equalTo("id", o.getId())
        .findAll()
        .first()
        .deleteFromRealm()).map(delete -> null);
  }
}
