package me.ndjoe.plantjournal2.data.handler;

import java.util.List;
import me.ndjoe.plantjournal2.data.model.BaseModel;
import rx.Observable;

public abstract class BaseHandler<T extends BaseModel> {

  abstract Observable<T> getObject(String id);

  abstract Observable<List<T>> listObjects();

  abstract Observable<T> createObject(T object);

  abstract Observable<T> updateObject(T object);

  abstract Observable<Void> deleteObject(T object);
}
