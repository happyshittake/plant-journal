package me.ndjoe.plantjournal2.data.handler;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.Sort;
import java.util.List;
import me.ndjoe.plantjournal2.data.model.Journal;
import me.ndjoe.plantjournal2.data.model.Plant;
import me.ndjoe.plantjournal2.data.service.JournalService;
import rx.Observable;

public class JournalHandler extends BaseRealmHandler<Journal> implements JournalService {
  protected Realm mRealm;

  public JournalHandler(Realm realm) {
    super(realm);
    mRealm = realm;
  }

  @Override public Class<Journal> getModelClass() {
    return Journal.class;
  }

  @Override
  public Observable<List<Journal>> getJournalsSortedHeightByPlant(Plant plant, Sort sort) {
    return mRealm.where(getModelClass())
        .findAllSorted("date", sort)
        .asObservable()
        .map(mRealm::copyFromRealm);
  }

  public Observable<RealmList<Journal>> addMultipleJournal(RealmList<Journal> journals) {
    return doAsync(journals, mRealm::copyToRealmOrUpdate);
  }

  @Override public Observable<List<Journal>> getJournalSortedDateByPlant(Plant plant, Sort sort) {
    return mRealm.where(getModelClass())
        .findAllSorted("height", sort)
        .asObservable()
        .map(mRealm::copyFromRealm);
  }

  @Override public Observable<Journal> addJournal(Journal journal) {
    return createObject(journal);
  }

  @Override public Observable<Journal> updateJournal(Journal journal) {
    return updateObject(journal);
  }

  @Override public Observable<Void> deleteJournal(Journal journal) {
    return deleteObject(journal);
  }
}
