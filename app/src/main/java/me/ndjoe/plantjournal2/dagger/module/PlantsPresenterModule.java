package me.ndjoe.plantjournal2.dagger.module;

import dagger.Module;
import dagger.Provides;
import me.ndjoe.plantjournal2.data.service.JournalService;
import me.ndjoe.plantjournal2.data.service.PlantService;
import me.ndjoe.plantjournal2.ui.plants.PlantsContract;
import me.ndjoe.plantjournal2.ui.plants.PlantsPresenter;

@Module public class PlantsPresenterModule {
  private PlantsContract.View mView;

  public PlantsPresenterModule(PlantsContract.View view) {
    mView = view;
  }

  @Provides PlantsContract.Presenter providePresenter(PlantService plantService,
      JournalService journalService) {
    return new PlantsPresenter(mView, plantService, journalService);
  }
}
