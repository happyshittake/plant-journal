package me.ndjoe.plantjournal2.dagger.component;

import dagger.Component;
import me.ndjoe.plantjournal2.dagger.AppScope;
import me.ndjoe.plantjournal2.dagger.module.PlantsPresenterModule;
import me.ndjoe.plantjournal2.ui.plants.PlantsFragment;

@AppScope @Component(modules = {
    PlantsPresenterModule.class
}, dependencies = ServicesComponent.class) public interface PlantComponent {
  void inject(PlantsFragment plantsFragment);
}
