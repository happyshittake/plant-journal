package me.ndjoe.plantjournal2.dagger.module;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import javax.inject.Singleton;
import me.ndjoe.plantjournal2.data.handler.JournalHandler;
import me.ndjoe.plantjournal2.data.handler.PlantHandler;
import me.ndjoe.plantjournal2.data.service.JournalService;
import me.ndjoe.plantjournal2.data.service.PlantService;

@Module public class HandlersModule {
  @Provides @Singleton PlantService providePlantService(Realm realm) {
    return new PlantHandler(realm);
  }

  @Provides @Singleton JournalService provideJournalService(Realm realm) {
    return new JournalHandler(realm);
  }
}
