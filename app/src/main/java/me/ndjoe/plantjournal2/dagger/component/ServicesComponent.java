package me.ndjoe.plantjournal2.dagger.component;

import dagger.Component;
import io.realm.Realm;
import javax.inject.Singleton;
import me.ndjoe.plantjournal2.dagger.module.HandlersModule;
import me.ndjoe.plantjournal2.dagger.module.RealmModule;
import me.ndjoe.plantjournal2.data.service.JournalService;
import me.ndjoe.plantjournal2.data.service.PlantService;
import me.ndjoe.plantjournal2.ui.plants.ActivityPlants;

@Singleton @Component(modules = {
    HandlersModule.class, RealmModule.class
}) public interface ServicesComponent {
  void inject(ActivityPlants activityPlants);

  PlantService plantService();

  JournalService journalService();

  Realm realm();
}
