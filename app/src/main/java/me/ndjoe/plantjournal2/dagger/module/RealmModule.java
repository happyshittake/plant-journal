package me.ndjoe.plantjournal2.dagger.module;

import android.app.Application;
import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import javax.inject.Singleton;

@Module public class RealmModule {
  private Application mApplication;

  public RealmModule(Application application) {
    mApplication = application;
  }

  @Provides @Singleton Realm provideRealm() {
    Realm.init(mApplication);
    RealmConfiguration configuration = new RealmConfiguration.Builder().build();
    Realm.setDefaultConfiguration(configuration);

    return Realm.getDefaultInstance();
  }
}
