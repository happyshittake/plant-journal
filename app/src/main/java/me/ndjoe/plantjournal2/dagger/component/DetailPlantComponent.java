package me.ndjoe.plantjournal2.dagger.component;

import dagger.Component;
import me.ndjoe.plantjournal2.dagger.AppScope;
import me.ndjoe.plantjournal2.dagger.module.DetailPlantPresenterModule;
import me.ndjoe.plantjournal2.ui.detailplant.DetailPlantActivity;

@AppScope
@Component(modules = DetailPlantPresenterModule.class, dependencies = ServicesComponent.class)
public interface DetailPlantComponent {
  void inject(DetailPlantActivity detailPlantActivity);
}
