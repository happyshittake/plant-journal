package me.ndjoe.plantjournal2.dagger;

import javax.inject.Scope;

@Scope public @interface AppScope {
}
