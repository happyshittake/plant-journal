package me.ndjoe.plantjournal2.dagger.module;

import dagger.Module;
import dagger.Provides;
import me.ndjoe.plantjournal2.data.service.JournalService;
import me.ndjoe.plantjournal2.data.service.PlantService;
import me.ndjoe.plantjournal2.ui.detailplant.DetailPlantContract;
import me.ndjoe.plantjournal2.ui.detailplant.DetailPlantPresenter;

@Module public class DetailPlantPresenterModule {
  private DetailPlantContract.View mView;

  public DetailPlantPresenterModule(DetailPlantContract.View view) {
    mView = view;
  }

  @Provides DetailPlantContract.Presenter providePresenter(PlantService plantService,
      JournalService journalService) {
    return new DetailPlantPresenter(mView, plantService, journalService);
  }
}
